Faker and json-server used for mock backend.  `see backend folder`

Start mock backend: `npm run server`

Start application: `ng serve --open` will open `http://localhost:4200/`

Run tests `ng test` (the service is unit tested)
