Generate mock data: (faker): `npm run generate`

Run backend mock (json-server): `npm run server`

http://localhost:3000/data

`GET /data`

`GET /data/<id>`

`POST /data`

`PUT /data/<id>`

`PATCH /data/<id>`

`DELETE /data/<id>`
