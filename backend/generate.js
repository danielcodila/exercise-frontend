var faker = require('faker');

var database = { data: []};

for (var i = 1; i<= 10; i++) {
  database.data.push({
    id: i,
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
  });
}

console.log(JSON.stringify(database));
