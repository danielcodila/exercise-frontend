import {TestBed} from '@angular/core/testing';

import {CrudService} from './crud.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {User} from './user';

describe('CrudService', () => {
  let httpTestingController: HttpTestingController;
  let service: CrudService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CrudService]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(CrudService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all users', () => {
    const users: User[] = [
      {id: 1, firstName: 'a', lastName: 'b', email: 'a@a.com'},
      {id: 2, firstName: 'c', lastName: 'd', email: 'a@b.com'}
    ];

    service.getAll().subscribe((data: User[]) => {
      expect(data).not.toBeNull();
      expect(JSON.stringify(data)).toEqual(JSON.stringify(users));
    });

    const testRequest = httpTestingController.expectOne(`http://localhost:3000/data`);
    expect(testRequest.request.method).toEqual('GET');

    testRequest.flush(users);
  });

  it('should get one user', () => {
    const user: User = {id: 1, firstName: 'a', lastName: 'b', email: 'a@a.com'};

    service.getById(1).subscribe((data: User) => {
      expect(data).not.toBeNull();
      expect(JSON.stringify(data)).toEqual(JSON.stringify(user));
    });

    const testRequest = httpTestingController.expectOne(`http://localhost:3000/data/1`);
    expect(testRequest.request.method).toEqual('GET');

    testRequest.flush(user);
  });

  it('should create user', () => {
    const user: User = {id: 1, firstName: 'a', lastName: 'b', email: 'a@a.com'};

    service.create(user).subscribe((data: User) => {
      expect(data.email).toEqual('a@a.com');
    });

    const testRequest = httpTestingController.expectOne(`http://localhost:3000/data/`);
    expect(testRequest.request.method).toEqual('POST');

    testRequest.flush(user);
  });

  it('should update user', () => {
    const user: User = {id: 1, firstName: 'update', lastName: 'b', email: 'a@a.com'};

    service.update(1, user).subscribe((data: User) => {
      expect(data.firstName).toEqual('update');
    });

    const testRequest = httpTestingController.expectOne(`http://localhost:3000/data/1`);
    expect(testRequest.request.method).toEqual('PUT');

    testRequest.flush(user);
  });

  it('should delete user', () => {
    const user: User = {id: 1, firstName: 'delete', lastName: 'b', email: 'a@a.com'};

    service.delete(1).subscribe((data: User) => {
      expect(data.id).toEqual(1);
    });

    const testRequest = httpTestingController.expectOne(`http://localhost:3000/data/1`);
    expect(testRequest.request.method).toEqual('DELETE');

    testRequest.flush(user);
  });

});
