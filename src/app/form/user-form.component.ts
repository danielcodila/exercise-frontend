import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {CrudService} from '../crud.service';
import {User} from '../user';

@Component({
  selector: 'app-second',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  userForm: FormGroup;
  user: User;
  updateUserWithId: number;
  buttonLabel: string;

  constructor(public formBuilder: FormBuilder,
              private router: Router,
              public crudService: CrudService) {

    this.getUserById();
  }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      email: ['']
    });
    this.buttonLabel = 'Create User';
  }

  private getUserById(): void {
    const selectedUser = this.router.getCurrentNavigation().extras.state;

    if (selectedUser) {
      this.crudService.getById(selectedUser.id).subscribe((data: User) => {
        this.userForm.patchValue(data);
        this.buttonLabel = 'Update User';
        this.updateUserWithId = data.id;
      });
    }
  }

  createUser(): void {
    this.crudService.create(this.userForm.value).subscribe(() => {
      this.router.navigateByUrl('/users');
    });
  }

  updateUser(id: number): void {
    this.crudService.update(id, this.userForm.value).subscribe(() => {
      this.router.navigateByUrl('/users');
    });
  }
}
