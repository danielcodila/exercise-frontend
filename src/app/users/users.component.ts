import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {CrudService} from '../crud.service';

@Component({
  selector: 'app-first',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];

  constructor(public crudService: CrudService) {
  }

  ngOnInit(): void {
    this.crudService.getAll().subscribe((data: User[]) => {
      this.users = data;
    });
  }

  deleteUser(id: number): void {
    this.crudService.delete(id).subscribe(res => {
      console.log('User with id:' + id + 'deleted!');
    });

    // update the list of users
    this.users = this.users.filter(user => user.id !== id);
  }

}
