import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {User} from './user';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  private apiServer = 'http://localhost:3000';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) {
  }

  create(user): Observable<User> {
    return this.httpClient.post<User>(this.apiServer + '/data/', JSON.stringify(user), this.httpOptions);

    // ToDo: error handling should be added..  instanceof ErrorEvent client side error else server side error
  }

  getById(id): Observable<User> {
    return this.httpClient.get<User>(this.apiServer + '/data/' + id);
  }

  getAll(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.apiServer + '/data');
  }

  update(id, user): Observable<User> {
    return this.httpClient.put<User>(this.apiServer + '/data/' + id, JSON.stringify(user), this.httpOptions);
  }

  delete(id): Observable<User> {
    return this.httpClient.delete<User>(this.apiServer + '/data/' + id, this.httpOptions);
  }
}
